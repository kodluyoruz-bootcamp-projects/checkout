package com.group1.checkout.externalservice;


import com.group1.checkout.contract.request.DecreaseQuantityRequest;
import com.group1.checkout.contract.request.response.ListingResponse;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
public class ListingExternalService {
    private final RestTemplate restTemplate;
    private HttpHeaders headers;
    @Value("${listing.host}")
    private String host;
    @Value("${listing.port}")
    private String port;

    public ListingExternalService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
        this.headers = new HttpHeaders();
        this.headers.setContentType(MediaType.APPLICATION_JSON);
    }

    public boolean decreaseStock(String listingId, int quantity) {
        boolean result = false;

        String url = "http://" + host + ":" + port + "/listings?id=" + listingId;
        try {
            JSONObject decreaseStock = new JSONObject();
            decreaseStock.put("quantity", quantity);

            HttpEntity<String> request =
                    new HttpEntity<String>(decreaseStock.toString(), headers);
            ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.PATCH, request, String.class);

            if (response.getStatusCode().equals(HttpStatus.ACCEPTED)) {
                result = true;
            }

        } catch (Exception exception) {

        }
        return result;
    }

    public List<ListingResponse> getListings(String listingIds) {

        String url = "http://" + host + ":" + port + "/listings?ids=" + listingIds;

        HttpEntity<String> request =
                new HttpEntity<String>(headers);

        ResponseEntity<ListingResponse[]> response = restTemplate.exchange(url, HttpMethod.GET, request, ListingResponse[].class);

        return Arrays.asList(response.getBody());
    }


}

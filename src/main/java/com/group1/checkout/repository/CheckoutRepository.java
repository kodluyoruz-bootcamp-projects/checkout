package com.group1.checkout.repository;

import com.couchbase.client.core.error.DocumentNotFoundException;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.Collection;
import com.couchbase.client.java.kv.GetResult;
import com.group1.checkout.domain.Cart;
import com.group1.checkout.exception.CartNotFoundException;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class CheckoutRepository {

    private final Cluster couchbaseCluster;
    private final Collection checkoutCollection;

    public CheckoutRepository(Cluster couchbaseCluster, Collection checkoutCollection) {
        this.couchbaseCluster = couchbaseCluster;
        this.checkoutCollection = checkoutCollection;
    }

    public void insert(Cart cart) {
        checkoutCollection.insert(cart.getId(), cart);
    }

    public void update(Cart cart) {
        checkoutCollection.replace(cart.getId(), cart);
    }

    public Cart findById(String id) {
        try {

            GetResult getResult = checkoutCollection.get(id);
            Cart cart = getResult.contentAs(Cart.class);
            return Optional.of(cart).get();

        } catch (DocumentNotFoundException exception) {
            throw new CartNotFoundException("Cart not found");
        }
    }



}

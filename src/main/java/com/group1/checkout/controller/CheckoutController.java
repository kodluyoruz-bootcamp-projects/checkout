package com.group1.checkout.controller;

import com.group1.checkout.contract.request.AddListingRequest;
import com.group1.checkout.contract.request.CheckoutRequest;
import com.group1.checkout.contract.request.CreateCartRequest;
import com.group1.checkout.contract.request.UpdateListingQuantityRequest;
import com.group1.checkout.contract.request.response.ListingResponse;
import com.group1.checkout.domain.Cart;
import com.group1.checkout.service.CheckoutService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/cart")
public class CheckoutController {

    private final CheckoutService checkoutService;

    public CheckoutController(CheckoutService checkoutService) {
        this.checkoutService = checkoutService;
    }


    @GetMapping("/{id}")
    public ResponseEntity getById(@PathVariable String id) {
        Cart cart = checkoutService.getCartByCartId(id);
        return ResponseEntity.ok(cart);
    }

    @PostMapping
    public ResponseEntity initCart(@RequestBody CreateCartRequest createCartRequest) {
        String id = checkoutService.create(createCartRequest);
        URI location = URI.create(String.format("/checkout/%s", id));
        return ResponseEntity.created(location).build();
    }

    @PostMapping("/listing")
    public ResponseEntity addListing(@RequestBody AddListingRequest addListingRequest) {
        checkoutService.addListingToCart(addListingRequest);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/checkout")
    public ResponseEntity checkOut(@RequestBody CheckoutRequest checkoutRequest) {
        List<String> succeedListings = checkoutService.checkout(checkoutRequest);
        return ResponseEntity.ok(succeedListings);
    }

    @GetMapping("/listing")
    public ResponseEntity getListingsByIdList(@RequestParam String ids) {
        List<ListingResponse> listings = checkoutService.getListingsByIdList(ids);
        return ResponseEntity.ok(listings);
    }
    @DeleteMapping("/{cartId}/listing/{listingId}")
    public ResponseEntity removeListingFromCart(@PathVariable String cartId, @PathVariable String listingId){
        checkoutService.removeListingFromCart(cartId, listingId);
        return ResponseEntity.ok().build();
    }
    @PatchMapping("/listing")
    public ResponseEntity updateListingQuantity(@RequestBody UpdateListingQuantityRequest updateListingQuantityRequest) {
        checkoutService.updateListingQuantity(updateListingQuantityRequest);
        return ResponseEntity.accepted().build();
    }


    }

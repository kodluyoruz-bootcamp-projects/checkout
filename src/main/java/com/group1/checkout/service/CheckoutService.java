package com.group1.checkout.service;

import com.group1.checkout.contract.request.AddListingRequest;
import com.group1.checkout.contract.request.CheckoutRequest;
import com.group1.checkout.contract.request.CreateCartRequest;
import com.group1.checkout.contract.request.UpdateListingQuantityRequest;
import com.group1.checkout.contract.request.response.ListingResponse;
import com.group1.checkout.domain.Cart;
import com.group1.checkout.domain.Listing;
import com.group1.checkout.exception.ListingNotFoundException;
import com.group1.checkout.externalservice.ListingExternalService;
import com.group1.checkout.repository.CheckoutRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CheckoutService {
    private final CheckoutRepository checkoutRepository;
    private final ListingExternalService listingExternalService;



    public CheckoutService(CheckoutRepository checkoutRepository, ListingExternalService listingExternalService) {

        this.checkoutRepository = checkoutRepository;
        this.listingExternalService = listingExternalService;
    }

    public Cart getCartByCartId(String id) {
        if (id == null || id.isEmpty()) {
            throw new IllegalArgumentException("Id list cannot be null or empty");
        }
        return checkoutRepository.findById(id);
    }

    public String create(CreateCartRequest createCartRequest) {
        ensureCreateCartRequestValid(createCartRequest);

        Cart cart = new Cart();
        cart.setUserId(createCartRequest.getUserId());
        checkoutRepository.insert(cart);
        return cart.getId();
    }

    public void addListingToCart(AddListingRequest addListingRequest) {
        ensureAddListRequestValid(addListingRequest);

        Cart cart = checkoutRepository.findById(addListingRequest.getCartId());
        Listing listing = new Listing(addListingRequest.getListingId(), addListingRequest.getQuantity());
        cart.addListing(listing);

        checkoutRepository.update(cart);
    }

    public List<String> checkout(CheckoutRequest checkoutRequest) {
        ensureCheckoutRequestValid(checkoutRequest);
        Cart cart = checkoutRepository.findById(checkoutRequest.getCartId());
        List<Listing> listingList = cart.getListings();
        List<String> succeededListings = new ArrayList<>();

        for (int i=0;i<listingList.size();i++) {
            boolean isSucceed = listingExternalService.decreaseStock(listingList.get(i).getId(), listingList.get(i).getQuantity());
            if (isSucceed) {
                succeededListings.add(listingList.get(i).getId());
                cart.removeListing(listingList.get(i).getId());
            }
            //if(cart.getListings().size() == 0) break;
        }
        checkoutRepository.update(cart);
        return succeededListings;
    }

    public List<ListingResponse> getListingsByIdList(String listingIds) {

        if (listingIds == null || listingIds.isEmpty()) {
            throw new IllegalArgumentException("Listing ids cannot be null or empty!");
        }
        return listingExternalService.getListings(listingIds);
    }


    public void removeListingFromCart(String cartId, String listingId) {
        Cart cart = checkoutRepository.findById(cartId);
        cart.removeListing(listingId);
        checkoutRepository.update(cart);
    }

    public void updateListingQuantity(UpdateListingQuantityRequest updateListingQuantityRequest) {
        Cart cart = checkoutRepository.findById(updateListingQuantityRequest.getCartId());
        Optional<Listing> listingStream = cart.getListings().stream()
                .filter(l -> l.getId().equals(updateListingQuantityRequest.getListingId()))
                .findFirst();
        if (listingStream.isPresent()) {
            Listing listing = listingStream.get();
            listing.setQuantity(updateListingQuantityRequest.getQuantity());

            cart.updateListing(listing);
            checkoutRepository.update(cart);
        } else {
            throw new ListingNotFoundException("Listing is not found in listings.");
        }
    }

    private void ensureCheckoutRequestValid(CheckoutRequest checkoutRequest) {
        if (checkoutRequest.getCartId() == null) {
            throw new IllegalArgumentException("Cart Id cannot be null!");
        }
    }

    private void ensureAddListRequestValid(AddListingRequest addListingRequest) {
        if (addListingRequest.getCartId() == null) {
            throw new IllegalArgumentException("Cart id cannot be null");
        }
        if (addListingRequest.getListingId() == null) {
            throw new IllegalArgumentException("Listing id cannot be null");
        }
    }

    private void ensureCreateCartRequestValid(CreateCartRequest createCartRequest) {
        if (createCartRequest.getUserId() == null) {
            throw new IllegalArgumentException("User id cannot be null");
        }
    }


}

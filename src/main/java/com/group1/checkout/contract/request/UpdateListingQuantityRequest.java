package com.group1.checkout.contract.request;

public class UpdateListingQuantityRequest {
    private String listingId;
    private String cartId;
    private int quantity;


    public String getListingId() {
        return listingId;
    }

    public String getCartId() {
        return cartId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setListingId(String listingId) {
        this.listingId = listingId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}

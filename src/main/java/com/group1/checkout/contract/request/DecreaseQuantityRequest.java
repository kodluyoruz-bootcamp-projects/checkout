package com.group1.checkout.contract.request;

public class DecreaseQuantityRequest {
    private int quantity;

    public DecreaseQuantityRequest(int quantity) {
        this.quantity = quantity;
    }
}

package com.group1.checkout.contract.request;

public class CheckoutRequest {
    private String cartId;

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }
}

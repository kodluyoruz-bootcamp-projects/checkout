package com.group1.checkout.contract.request;

import com.group1.checkout.domain.Listing;

import java.util.List;

public class CreateCartRequest {
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}

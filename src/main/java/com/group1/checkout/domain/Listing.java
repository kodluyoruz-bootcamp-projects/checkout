package com.group1.checkout.domain;

public class Listing {
    private String id;
    private int quantity;

    public Listing(String id, int quantity) {
        this.id = id;
        this.quantity = quantity;
    }

    public Listing() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    //public void increaseQuantity (){ this.quantity++; }
    //public void decreaseQuantity (){ this.quantity--; }
}

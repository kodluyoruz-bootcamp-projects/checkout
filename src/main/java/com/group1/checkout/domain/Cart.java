package com.group1.checkout.domain;

import com.group1.checkout.exception.ListingNotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Cart {
    private String id;
    private String userId;
    private List<Listing> listings;

    public Cart() {
        this.id = UUID.randomUUID().toString();
        this.listings = new ArrayList<>();
    }

    public Cart(String id, String userId, List<Listing> listings) {
        this.id = id;
        this.userId = userId;
        this.listings = listings;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<Listing> getListings() {
        return listings;
    }

    public void setListings(List<Listing> listings) {
        this.listings = listings;
    }

    public void addListing(Listing listing) {
        this.listings.add(listing);
    }

    public void updateListing(Listing listingToUpdate) {
        for (Listing listing : this.listings) {
            if (listing.getId() == listingToUpdate.getId()) {
                int idx = this.listings.indexOf(listing);
                listings.set(idx, listingToUpdate);
                return;
            }
        }
        throw new ListingNotFoundException("Listing not found");
    }

    public void removeListing(String listingId) {
        boolean isRemoved = listings.removeIf(l -> l.getId().equals(listingId));
        if (!isRemoved) {
            throw new ListingNotFoundException("Listing not found!");
        }
    }
}

package com.group1.checkout;

import com.group1.checkout.contract.request.AddListingRequest;
import com.group1.checkout.contract.request.CheckoutRequest;
import com.group1.checkout.contract.request.CreateCartRequest;
import com.group1.checkout.contract.request.UpdateListingQuantityRequest;
import com.group1.checkout.domain.Cart;
import com.group1.checkout.domain.Listing;
import com.group1.checkout.exception.CartNotFoundException;
import com.group1.checkout.exception.ListingNotFoundException;
import com.group1.checkout.externalservice.ListingExternalService;
import com.group1.checkout.repository.CheckoutRepository;
import com.group1.checkout.service.CheckoutService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CheckoutServiceTests {

    @Mock
    private CheckoutRepository checkoutRepository;
    @Mock
    private ListingExternalService listingExternalService;

    @InjectMocks
    private CheckoutService checkoutService;

    @Test
    public void getCartByCartId_should_throw_CartNotFoundException_when_there_is_no_cart() {
        //Arrange
        String id = "1";
        when(checkoutRepository.findById(id)).thenThrow(new CartNotFoundException("not found!"));
        //Act
        Throwable throwable = catchThrowable(() -> checkoutService.getCartByCartId(id));
        // Assert
        assertThat(throwable).isInstanceOf(CartNotFoundException.class).hasMessage("not found!");
    }
    @Test
    public void getCartByCartId_should_throw_IllegalArgumentException_when_id_empty() {
        //Arrange
        String id = "";
        //Act
        Throwable throwable = catchThrowable(() -> checkoutService.getCartByCartId(id));

        // Assert
        assertThat(throwable).isInstanceOf(IllegalArgumentException.class);
    }
    @Test
    public void getCartByCartId_should_throw_IllegalArgumentException_when_id_null() {
        //Arrange
        String id = null;
        //Act
        Throwable throwable = catchThrowable(() -> checkoutService.getCartByCartId(id));

        // Assert
        assertThat(throwable).isInstanceOf(IllegalArgumentException.class);
    }
    @Test
    public void create_should_throw_IllegalArgumentException_when_userId_is_empty() {
        //Arrange
        CreateCartRequest createCartRequest = new CreateCartRequest();
        //Act
        Throwable throwable = catchThrowable(() -> checkoutService.create(createCartRequest));
        // Assert
        assertThat(throwable).isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void create_should_called_when_createCartRequest_is_valid() {
        //Arrange
        CreateCartRequest createCartRequest = new CreateCartRequest();
        createCartRequest.setUserId("id");
        //Act
        checkoutService.create(createCartRequest);
        //Assert
        verify(checkoutRepository, times(1)).insert(any(Cart.class));
    }
    @Test
    public void addListingToCart__should_throw_IllegalArgumentException_when_cartId_is_empty() {
        //Arrange
        AddListingRequest addListingRequest = new AddListingRequest();
        addListingRequest.setListingId("id");
        addListingRequest.setQuantity(1);
        //Act
        Throwable throwable = catchThrowable(() -> checkoutService.addListingToCart(addListingRequest));
        // Assert
        assertThat(throwable).isInstanceOf(IllegalArgumentException.class);
    }
    @Test
    public void addListingToCart__should_throw_IllegalArgumentException_when_listingId_is_empty() {
        //Arrange
        AddListingRequest addListingRequest = new AddListingRequest();
        addListingRequest.setCartId("id");
        addListingRequest.setQuantity(1);
        //Act
        Throwable throwable = catchThrowable(() -> checkoutService.addListingToCart(addListingRequest));
        // Assert
        assertThat(throwable).isInstanceOf(IllegalArgumentException.class);
    }
    @Test
    public void addListingToCart_should_called_when_addListingRequest_is_valid() {
        //Arrange
        List<Listing> listings = new ArrayList<>();
        when(checkoutRepository.findById(any())).thenReturn(new Cart("1","2", listings));

        AddListingRequest addListingRequest = new AddListingRequest();
        addListingRequest.setCartId("id");
        addListingRequest.setQuantity(1);
        addListingRequest.setListingId("id");
        //Act
        checkoutService.addListingToCart(addListingRequest);
        //Assert
        verify(checkoutRepository, times(1)).update(any(Cart.class));
    }

    @Test
    public void checkout_should_called_when_checkoutRequest_is_valid() {
        //Arrange
        CheckoutRequest checkoutRequest = new CheckoutRequest();
        checkoutRequest.setCartId("id");
        List<Listing> listings = new ArrayList<>();
        when(checkoutRepository.findById(any())).thenReturn(new Cart("1","2", listings));
        //when(listingExternalService.decreaseStock(any())).thenReturn(true);

        //Act
        checkoutService.checkout(checkoutRequest);
        // Assert
        verify(checkoutRepository, times(1)).update(any(Cart.class));
    }
    @Test
    public void removeListingFromCart_should_throw_exception_when_there_is_no_listing_to_remove() {
        //Arrange
        String listingId = "listingId";
        List<Listing> listings = new ArrayList<>();
        listings.add(new Listing(listingId,1));
        when(checkoutRepository.findById(any())).thenReturn(new Cart("1","2", listings));
        //Act
        Throwable throwable = catchThrowable(() -> checkoutService.removeListingFromCart("id","listingId1"));
        //Assert
        assertThat(throwable).isInstanceOf(ListingNotFoundException.class);

    }
    @Test
    public void removeListingFromCart_should_called_update(){
        //Arrange
        String listingId = "listingId";
        List<Listing> listings = new ArrayList<>();
        listings.add(new Listing(listingId,1));
        when(checkoutRepository.findById(any())).thenReturn(new Cart("1","2", listings));
        //Act
        checkoutService.removeListingFromCart("id",listingId);
        //Assert
        verify(checkoutRepository, times(1)).update(any(Cart.class));
    }
    @Test
    public void updateListingQuantity_should_throw_not_found_exception_when_requested_listing_not_exists(){
        //Arrange
        String listingId = "listingId";
        String cartId = "cartId";
        List<Listing> listings = new ArrayList<>();
        listings.add(new Listing(listingId,1));
        when(checkoutRepository.findById(any())).thenReturn(new Cart(cartId,"2", listings));
        UpdateListingQuantityRequest updateListingQuantityRequest = new UpdateListingQuantityRequest();
        updateListingQuantityRequest.setCartId(cartId);
        updateListingQuantityRequest.setListingId("1");
        updateListingQuantityRequest.setQuantity(1);
        //Act
        Throwable throwable = catchThrowable(() -> checkoutService.updateListingQuantity(updateListingQuantityRequest));
        //Assert
        assertThat(throwable).isInstanceOf(ListingNotFoundException.class);
    }
    @Test
    public void updateListingQuantity_should_call_update(){
        //Arrange
        String listingId = "listingId";
        String cartId = "cartId";
        List<Listing> listings = new ArrayList<>();
        listings.add(new Listing(listingId,1));
        when(checkoutRepository.findById(any())).thenReturn(new Cart(cartId,"2", listings));
        UpdateListingQuantityRequest updateListingQuantityRequest = new UpdateListingQuantityRequest();
        updateListingQuantityRequest.setCartId(cartId);
        updateListingQuantityRequest.setListingId(listingId);
        updateListingQuantityRequest.setQuantity(2);
        //Act
        checkoutService.updateListingQuantity(updateListingQuantityRequest);
        //Assert
        verify(checkoutRepository, times(1)).update(any(Cart.class));
    }
    }

package com.group1.checkout;

import com.group1.checkout.domain.Cart;
import com.group1.checkout.domain.Listing;
import com.group1.checkout.exception.ListingNotFoundException;
import com.group1.checkout.externalservice.ListingExternalService;
import org.junit.Test;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class CheckoutTests {
    @Test
    public void cart_should_add_listing_to_listings(){
        //Arrange
        List<Listing> listings = new ArrayList<>();
        listings.add(new Listing("listing1",1));
        Cart cart = new Cart("id","userId",listings);
        //Act
        Listing listingToAdd = new Listing("listing2",2);
        listings.add(new Listing());
        cart.addListing(listingToAdd);
        //Assert
        assertEquals(listings, cart.getListings());
    }
    @Test
    public void cart_should_update_listing(){
        //Arrange
        List<Listing> listings = new ArrayList<>();
        listings.add(new Listing("listing1",1));
        Cart cart = new Cart("id","userId",listings);
        //Act
        Listing listingToChange = new Listing("listing1",5);
        cart.updateListing(listingToChange);
        //Assert
        assertEquals(5,cart.getListings().get(0).getQuantity());
    }
    @Test
    public void cart_should_throw_exception_when_no_listing_to_update_found(){
        //Arrange
        List<Listing> listings = new ArrayList<>();
        listings.add(new Listing("listing2",1));
        Cart cart = new Cart("id","userId",listings);
        //Act
        Listing listingToChange = new Listing("listing1",5);
        Throwable throwable = catchThrowable(() -> cart.updateListing(listingToChange));
        // Assert
        assertThat(throwable).isInstanceOf(ListingNotFoundException.class);
    }
    @Test
    public void cart_should_throw_exception_when_no_listing_to_delete_found(){
        //Arrange
        List<Listing> listings = new ArrayList<>();
        listings.add(new Listing("listing2",1));
        Cart cart = new Cart("id","userId",listings);
        //Act
        Throwable throwable = catchThrowable(() -> cart.removeListing("listing1"));
        // Assert
        assertThat(throwable).isInstanceOf(ListingNotFoundException.class);
    }
    @Test
    public void cart_should_remove_listing(){
        //Arrange
        String listingId = "listingId";
        List<Listing> listings = new ArrayList<>();
        listings.add(new Listing(listingId,1));
        Cart cart = new Cart("id","userId",listings);
        //Act
        cart.removeListing(listingId);
        //Arrange
        assertEquals(0,cart.getListings().size());
    }
}
